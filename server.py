#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @FILE : `server.py`
# @CREATION TIME : `2018-12-17 10:22`
# @SCRIPT EDITOR : `PyCharm`
# @AUTHOR : `SACHAYURI`

import socket
import threading


IP_ADDRESS = '0.0.0.0'
PORT = 12388
ADDRESS = (IP_ADDRESS, PORT)
BUFFER_SIZE = 2048
MAX_CONNECT_COUNT = 10


class ChatManager:

    vector = []

    def __init__(self):
        pass

    @staticmethod
    def get_chat_manager():
        return ChatManager()

    def add(self, cs, addr):
        self.vector.append((cs, addr))

    def publish(self, cs, data: str):
        """
        @:param cs
            会话socket
        @:param data
            需要送的数据
        """
        cs.previous_message = data
        cs.message_out(data, )


class ChatSocket(threading.Thread):

    def __init__(self, sock, addr, buffer_size):
        super().__init__()
        self._sock = sock
        self._addr = addr
        self._client_receive = None
        self._buffer_size = buffer_size
        # self._client_ip_addr = addr[0]
        # self._client_port = 12388
        self.previous_message = ""

    # 接收信息
    def message_in(self):
        self._client_receive = bytes(self._sock.recv(self._buffer_size)).decode("utf-8")

    # 发送信息
    def message_out(self, data: str, addr):
        self._sock.sendto(data.encode("utf-8"), addr)
        # self._sock.send(data.encode("utf-8"), (self._client_ip_addr,self._client_port))

    def run(self):
        # 监听socket会话
        while True:
            try:
                line = self._sock.recv(self._buffer_size).decode("utf-8")
                if line:
                    print(line)
                ChatManager.get_chat_manager().publish(self, line)
                # line = ''
            except ConnectionResetError:
                print("%s closed!" % self._addr)


class Listener(threading.Thread):

    def __init__(self, address, max_conn, buffer_size):
        super().__init__()
        self._tcp_server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._address = address
        self.max_conn = max_conn
        self._buffer_size = buffer_size

    def run(self):
        server_socket = self._tcp_server_socket
        server_socket.bind(self._address)
        server_socket.listen(5)
        chat_manager = ChatManager.get_chat_manager()

        while True:
            user_sock, addr = server_socket.accept()
            print(addr, "connected")
            chat_sock = ChatSocket(user_sock, addr, self._buffer_size)
            chat_sock.start()
            chat_manager.add(chat_sock, addr)
            user_id = len(chat_manager.vector)
            print("\r current connected users: %s" % user_id, end='')


def main():
    global ADDRESS, BUFFER_SIZE
    print("server running...")
    Listener(
        ADDRESS,
        MAX_CONNECT_COUNT,
        BUFFER_SIZE
    ).start()


if __name__ == '__main__':
    main()
