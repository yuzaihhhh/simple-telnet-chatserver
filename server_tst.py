#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @FILE : `server_tst.py`
# @CREATION TIME : `2018-12-17 21:27 `
# @SCRIPT EDITOR : `PyCharm`
# @AUTHOR : `SACHAYURI`

# import socket
# s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# # s.bind(('0.0.0.0', 12387))
# # 建立连接:
# s.connect(('127.0.0.1', 9999))
# # 接收欢迎消息:
# print(s.recv(2048).decode('utf-8'))
# while True:
#     # 发送数据:
#     data = input()
#     if data == 'exit':
#         break
#     s.sendto(data.encode('utf-8'), ('127.0.0.1', 12388))
#     print(s.recv(2048).decode('utf-8'))
#
# s.close()



import socket
import threading

class Server(threading.Thread):
    def run(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind(('0.0.0.0', 8888))
        s.listen(3)
        print("server running")
        sock, addr = s.accept()
        print(addr)
        s.send(b"server: hello!")
        content = s.recv(1024).decode("utf-8")
        print("server: " + content)


def main():
    Server().start()


if __name__ == '__main__':
    main()

# import socket
# import threading
# import time
#
# s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#
# # 监听端口:
# s.bind(('127.0.0.1', 9999))
#
# s.listen(5)
# print('Waiting for connection...')
#
#
# def tcplink(sock, addr):
#     print('Accept new connection from %s:%s...' % addr)
#     sock.send(b'Welcome!')
#     while True:
#         data = sock.recv(1024)
#         # time.sleep(1)
#         if not data or data.decode('utf-8') == 'exit':
#             break
#         sock.send(('Hello, %s!' % data.decode('utf-8')).encode('utf-8'))
#     sock.close()
#     print('Connection from %s:%s closed.' % addr)
#
#
# while True:
#     # 接受一个新连接:
#     sock, addr = s.accept()
#     # 创建新线程来处理TCP连接:
#     t = threading.Thread(target=tcplink, args=(sock, addr))
#     t.start()
